#ifndef SETOFPOINTS_H
#define SETOFPOINTS_H
#include "Point.h"

class Board;    //ZAPOWIEDŹ

//template <class T1,class T2, class T3>  //ZAPOWIEDŹ
//class SetOfPoints;

template <class T1,class T2, class T3>
struct Element
{
    //friend class Board;
    //friend class SetOfPoints<T1,T2,T3>;
    Point<T1, T2, T3> point;
    Element *next;
};

template <class T1,class T2, class T3>
class SetOfPoints
{
    friend class Board;     //FRIEND CLASS BOARD KLASA ZAPRZYJAŹNIONA
    private:
    Element<T1, T2, T3> *head, *tail;
    public:
    //CONSTRUCTOR
    SetOfPoints();
    //FUNCTIONS
    void addPoint(Point<T1, T2, T3> p);
    void removePoint(Point<T1, T2, T3> p);
    Point<T1, T2, T3>* findPoint(T1 x, T2 y);
    //OPERATORS
    friend    std::ostream& operator<<(std::ostream& os, const SetOfPoints<T1, T2, T3>& s)
    {
        Element<T1, T2, T3> *target=s.head;
        while(target!=NULL)
        {
            os << target->point <<std::endl;
            target=target->next;
        }
        return os;
    };

    SetOfPoints<T1, T2, T3> operator+(const SetOfPoints<T1, T2, T3>&) const;
    SetOfPoints<T1, T2, T3> operator*(const SetOfPoints<T1, T2, T3>&) const;
    SetOfPoints<T1, T2, T3> operator+=(const SetOfPoints<T1, T2, T3>&);
    SetOfPoints<T1, T2, T3> operator*=(const SetOfPoints<T1, T2, T3>&);
    //Point<T1, T2, T3> operator*(const Point<T1, T2, T3>&) const;
    //Point operator+=(const Point&);
    //Point operator*=(const Point&);
    ~SetOfPoints();
};

#endif // SETOFPOINTS_H

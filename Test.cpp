#include "Test.h"
#include <iostream>
#include "Point.h"
#include "SetOfPoints.h"

Test::Test()
{
}

void Test::pointOperators(void)
{
    std::cout << "Test of operators of Point class:" << std::endl;
    Point<float, float, float> p1(1,2,3);
    std::cout << "p1=\t\t" << p1 << std::endl;
    Point<float, float, float> p2(10,20,30);
    std::cout << "p2=\t\t" << p2 << std::endl;
    Point<float, float, float> p3=p1+p2;
    std::cout << "p3=p1+p2\t" << p3 << std::endl;
    p3=p1-p2;
    std::cout << "p3=p1-p2\t" << p3 << std::endl;
    p3+=p1;
    std::cout << "p3+=p1\t\t" << p3 << std::endl;
    p3-=p2;
    std::cout << "p3-=p2\t\t" << p3 << std::endl;
    p3=p2;
    std::cout << "p3=p2\t\t" << p3 << std::endl;
    bool b;
    b=p3==p2;
    std::cout << "p3==p2\t\t" << b << std::endl;
    b=p3!=p2;
    std::cout << "p3!=p2\t\t" << b << std::endl;
    b=p1==p2;
    std::cout << "p1==p2\t\t" << b << std::endl;
    b=p1!=p2;
    std::cout << "p1!=p2\t\t" << b << std::endl;
    b=p1>p2;
    std::cout << "p1>p2\t\t" << b << std::endl;
    b=p1<p2;
    std::cout << "p1<p2\t\t" << b << std::endl;
}

void Test::setOfPoints(void)
{
    Point<float, float, float> p1(2,1,2);
    Point<float, float, float> p2(2,0,2);
    Point<float, float, float> p3(-1,0,7);
    Point<float, float, float> p4(8,2,1);
    Point<float, float, float> p5(2,-1,7);
    Point<float, float, float> p6(2,0,2);
    Point<float, float, float> p7(8,2,1);
    std::cout <<p1<<"  "<<p2<<"  "<<p3<<"  "<<p4<<"  "<<p5<<"  "<<p6<<"  "<<p7<<"  "<< std::endl;
    SetOfPoints<float, float, float> s1;
    s1.addPoint(p1);
    s1.addPoint(p2);
    s1.addPoint(p3);
    s1.addPoint(p4);
    s1.addPoint(p5);
    s1.addPoint(p6);
    s1.addPoint(p7);
    std::cout <<"set1:"<< std::endl;
    std::cout <<s1<< std::endl;
    s1.removePoint(p3);
    std::cout <<"set1 - "<< p3 << std::endl;
    std::cout <<s1<< std::endl;
    Point<float, float, float> p8(4,1,9);
    s1.removePoint(p8);
    std::cout <<"set1 - "<< p8 <<  std::endl;
    std::cout <<s1<< std::endl;
    s1.removePoint(p1);
    std::cout <<"set1 - "<<  p1 <<  std::endl;
    std::cout <<s1<< std::endl;
    s1.addPoint(p1);
    s1.addPoint(p3);
    std::cout <<"set1:"<< std::endl;
    std::cout <<s1<< std::endl;
    Point<float, float, float> p11(2,1,2);
    Point<float, float, float> p12(5,-3,2);
    Point<float, float, float> p13(-1,0,7);
    Point<float, float, float> p14(8,2,1);
    Point<float, float, float> p15(2,0,7);
    Point<float, float, float> p16(2,0,2);
    Point<float, float, float> p17(7,7,-1);
    std::cout <<p11<<"  "<<p12<<"  "<<p13<<"  "<<p14<<"  "<<p15<<"  "<<p16<<"  "<<p17<<"  "<< std::endl;
    SetOfPoints<float, float, float> s2;
    s2.addPoint(p11);
    s2.addPoint(p12);
    s2.addPoint(p13);
    s2.addPoint(p14);
    s2.addPoint(p15);
    s2.addPoint(p16);
    s2.addPoint(p17);
    std::cout <<"set2:"<< std::endl;
    std::cout <<s2<< std::endl;
    SetOfPoints<float, float, float> s3=s1+s2;
    std::cout <<"set3=s1+s2"<< std::endl;
    std::cout <<s3<< std::endl;
    SetOfPoints<float, float, float> s4=s1*s2;
    std::cout <<"set4=s1*s2"<< std::endl;
    std::cout <<s4<< std::endl;

}

Test::~Test()
{
}

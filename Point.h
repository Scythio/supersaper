#ifndef POINT_H
#define POINT_H
#include <iostream>

template <class T1,class T2, class T3>  //ZAPOWIEDŹ
class SetOfPoints;

//template <class T1,class T2, class T3>  //ZAPOWIEDŹ
//struct Element;

class Board;    //ZAPOWIEDŹ

template <class T1,class T2,class T3>
class Point
{
    friend class SetOfPoints<T1,T2,T3>; //FRIEND CLASS SETOFPOINTS KLASA ZAPRZYJAŹNIONA
    friend class Board;
    //friend struct Element<T1,T2,T3>;

    private:
        T1 x;
        T2 y;
        T3 z;
        //Point* next;
    public:
        //CONSTRUCTORS
        Point<T1,T2,T3>(T1 xx=0,T2 yy=0,T3 zz=0);
        //FUNCTIONS
        void modify(T1 xx,T2 yy,T3 zz);
        //OPERATORS
        Point<T1,T2,T3> operator+(const Point<T1,T2,T3>&) const;
        Point<T1,T2,T3> operator-(const Point<T1,T2,T3>&) const;
        Point<T1,T2,T3> operator+=(const Point<T1,T2,T3>&);
        Point<T1,T2,T3> operator-=(const Point<T1,T2,T3>&);
        Point<T1,T2,T3> operator=(const Point<T1,T2,T3>&);
        bool operator==(const Point<T1,T2,T3>&);
        bool operator!=(const Point<T1,T2,T3>&);
        bool operator>(const Point<T1,T2,T3>&);
        bool operator<(const Point<T1,T2,T3>&);

        friend std::ostream& operator<<(std::ostream& os, const Point<T1,T2,T3>& p)
        {
            os << p.x << ' ' << p.y << ' ' << p.z;
            return os;
        }
        //DESTRUCTOR
        virtual ~Point();

    //protected:


};

#endif // POINT_H

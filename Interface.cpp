#include "Interface.h"
#include <iostream>
//#include <sstream>
#include <limits>

Interface::Interface()
{

}
void Interface::menu()
{
    std::cout<< "Menu" << std::endl;
}
void Interface::start()
{
    bool ready = false;
    int r, c;
    do{
        std::cout<< "Enter size of Minefield:" << std::endl;
        std::cout<< "rows  *space*  columns  *enter*" << std::endl;
        std::cin>> r >> c;
        if (r>4 && r<100 && c>4 && c<100)
        {
            row=r;
            col=c;
            ready=true;
        }
        else
        {
            std::cout<< "number of rows/columns must be intiger between 4 and 100" << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            /*char c;
            do
            {
                c = getchar();
            }
            while(c != '\n' && c != EOF);*/
        }
    }while(ready==false);
}
void Interface::control()
{
    bool ready = false;
    int xx, yy;
    char t;
    do{
        std::cout<< "Enter coordinates and task:" << std::endl;
        std::cout<< "Tasks:" << std::endl;
        std::cout<< "s - this place is Save" << std::endl;
        std::cout<< "! - mine here!" << std::endl;
        std::cout<< "? - not shure yet..." << std::endl;
        std::cout<< "row number  *space*  column number  *space*  task  *enter*" << std::endl;
        std::cin>> xx >> yy >> t;
        if (xx>=0 && xx<row && yy>=0 && yy<col)
        {
            if(t=='s' || t=='!' || t=='?' || t=='m')
            {
                x = xx;
                y = yy;
                task = t;
                ready = true;
            }
            else
            {
                std::cin.clear();
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            }
        }
        else
        {
            std::cout<< "number of rows/columns must be intiger between 4 and 100" << std::endl;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }while(ready==false);
}

bool Interface::theEnd(bool win)
{
    bool ready = false;
    char a;
    std::cout<<std::endl;
    if(win)
    {
        std::cout<< "YOU WON CONGRATS :D" << std::endl;
    }
    else
    {
        std::cout<< "OPSIE :(" << std::endl;
    }
    std::cout<<std::endl;
    std::cout<< "Do you want to play again?" << std::endl;
    std::cout<< "y - yes    n - no" << std::endl;
    do
    {
        std::cin>> a;
        if(a=='y' || a=='n')
        {
            if(a=='y')
            {
            return 1;
            }
            else
            {
            return 0;
            }
            ready = true;
        }
        else
        {
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }while(ready == false);
    return 0;
}


Interface::~Interface()
{
    //dtor
}

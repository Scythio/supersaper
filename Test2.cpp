#include "Test2.h"
#include <iostream>
#include "Point.h"
#include "SetOfPoints.h"

Test2::Test2()
{
}

void Test2::pointOperators(void)
{
    std::cout << "Test of operators of Point<char, char, char> class:" << std::endl;
    Point<char, char, char> p1('1','2','3');
    std::cout << "p1=\t\t" << p1 << std::endl;
    Point<char, char, char> p2('a','b','c');
    std::cout << "p2=\t\t" << p2 << std::endl;
    Point<char, char, char> p3=p1+p2;
    std::cout << "p3=p1+p2\t" << p3 << std::endl;
    p3=p1-p2;
    std::cout << "p3=p1-p2\t" << p3 << std::endl;
    p3+=p1;
    std::cout << "p3+=p1\t\t" << p3 << std::endl;
    p3-=p2;
    std::cout << "p3-=p2\t\t" << p3 << std::endl;
    p3=p2;
    std::cout << "p3=p2\t\t" << p3 << std::endl;
    bool b;
    b=p3==p2;
    std::cout << "p3==p2\t\t" << b << std::endl;
    b=p3!=p2;
    std::cout << "p3!=p2\t\t" << b << std::endl;
    b=p1==p2;
    std::cout << "p1==p2\t\t" << b << std::endl;
    b=p1!=p2;
    std::cout << "p1!=p2\t\t" << b << std::endl;
    b=p1>p2;
    std::cout << "p1>p2\t\t" << b << std::endl;
    b=p1<p2;
    std::cout << "p1<p2\t\t" << b << std::endl;
}

void Test2::setOfPoints(void)
{
    Point<char, char, char> p1('1','2','3');
    Point<char, char, char> p2('a','b','c');
    Point<char, char, char> p3('x','y','z');
    Point<char, char, char> p4('0','0','0');
    Point<char, char, char> p5('?','!','*');
    Point<char, char, char> p6('M','N','W');
    Point<char, char, char> p7('.',',',':');
    std::cout <<p1<<"  "<<p2<<"  "<<p3<<"  "<<p4<<"  "<<p5<<"  "<<p6<<"  "<<p7<<"  "<< std::endl;
    SetOfPoints<char, char, char> s1;
    s1.addPoint(p1);
    s1.addPoint(p2);
    s1.addPoint(p3);
    s1.addPoint(p4);
    s1.addPoint(p5);
    s1.addPoint(p6);
    s1.addPoint(p7);
    std::cout <<"set1:"<< std::endl;
    std::cout <<s1<< std::endl;
    s1.removePoint(p3);
    std::cout <<"set1 - "<< p3 << std::endl;
    std::cout <<s1<< std::endl;
    Point<char, char, char> p8('A','B','C');
    s1.removePoint(p8);
    std::cout <<"set1 - "<< p8 <<  std::endl;
    std::cout <<s1<< std::endl;
    s1.removePoint(p1);
    std::cout <<"set1 - "<<  p1 <<  std::endl;
    std::cout <<s1<< std::endl;
    s1.addPoint(p1);
    s1.addPoint(p3);
    std::cout <<"set1:"<< std::endl;
    std::cout <<s1<< std::endl;
    Point<char, char, char> p11('H','2','O');
    Point<char, char, char> p12('1','2','3');
    Point<char, char, char> p13('.',',',':');
    Point<char, char, char> p14('i','j','k');
    Point<char, char, char> p15('q','r','s');
    Point<char, char, char> p16('o','O','0');
    Point<char, char, char> p17('x','y','z');
    std::cout <<p11<<"  "<<p12<<"  "<<p13<<"  "<<p14<<"  "<<p15<<"  "<<p16<<"  "<<p17<<"  "<< std::endl;
    SetOfPoints<char, char, char> s2;
    s2.addPoint(p11);
    s2.addPoint(p12);
    s2.addPoint(p13);
    s2.addPoint(p14);
    s2.addPoint(p15);
    s2.addPoint(p16);
    s2.addPoint(p17);
    std::cout <<"set2:"<< std::endl;
    std::cout <<s2<< std::endl;
    SetOfPoints<char, char, char> s3=s1+s2;
    std::cout <<"set3=s1+s2"<< std::endl;
    std::cout <<s3<< std::endl;
    SetOfPoints<char, char, char> s4=s1*s2;
    std::cout <<"set4=s1*s2"<< std::endl;
    std::cout <<s4<< std::endl;

}

Test2::~Test2()
{
}

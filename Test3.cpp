#include "Test3.h"
#include <iostream>
#include "Point.h"
#include "SetOfPoints.h"

Test3::Test3()
{
}

void Test3::pointFunctions(void)
{
    Point<int, int, char> p1(1,2,'a');
    std::cout << "p1=\t\t" << p1 << std::endl;
    p1.modify(4,5,'m');
    std::cout << "\"p1.modify(4,5,'m');\"" << std::endl;
    std::cout << "modified p1=\t\t" << p1 << std::endl;
}

void Test3::pointOperators(void)
{
    std::cout << "Test of operators of Point<int, int, char> class:" << std::endl;
    Point<int, int, char> p1(1,2,'a');
    std::cout << "p1=\t\t" << p1 << std::endl;
    Point<int, int, char> p2(10,20,'b');
    std::cout << "p2=\t\t" << p2 << std::endl;
    Point<int, int, char> p3=p1+p2;
    std::cout << "p3=p1+p2\t" << p3 << std::endl;
    p3=p1-p2;
    std::cout << "p3=p1-p2\t" << p3 << std::endl;
    p3+=p1;
    std::cout << "p3+=p1\t\t" << p3 << std::endl;
    p3-=p2;
    std::cout << "p3-=p2\t\t" << p3 << std::endl;
    p3=p2;
    std::cout << "p3=p2\t\t" << p3 << std::endl;
    bool b;
    b=p3==p2;
    std::cout << "p3==p2\t\t" << b << std::endl;
    b=p3!=p2;
    std::cout << "p3!=p2\t\t" << b << std::endl;
    b=p1==p2;
    std::cout << "p1==p2\t\t" << b << std::endl;
    b=p1!=p2;
    std::cout << "p1!=p2\t\t" << b << std::endl;
    b=p1>p2;
    std::cout << "p1>p2\t\t" << b << std::endl;
    b=p1<p2;
    std::cout << "p1<p2\t\t" << b << std::endl;
}

void Test3::setOfPoints(void)
{
    Point<int, int, char> p1(1,2,'a');
    Point<int, int, char> p2('a','v','b');
    Point<int, int, char> p3(-1,22,'z');
    Point<int, int, char> p4(0,0,'0');
    Point<int, int, char> p5(0,1,'*');
    Point<int, int, char> p6(1,0,'W');
    Point<int, int, char> p7(3,5,':');
    std::cout <<p1<<"  "<<p2<<"  "<<p3<<"  "<<p4<<"  "<<p5<<"  "<<p6<<"  "<<p7<<"  "<< std::endl;
    SetOfPoints<int, int, char> s1;
    s1.addPoint(p1);
    s1.addPoint(p2);
    s1.addPoint(p3);
    s1.addPoint(p4);
    s1.addPoint(p5);
    s1.addPoint(p6);
    s1.addPoint(p7);
    std::cout <<"set1:"<< std::endl;
    std::cout <<s1<< std::endl;
    s1.removePoint(p3);
    std::cout <<"set1 - "<< p3 << std::endl;
    std::cout <<s1<< std::endl;
    Point<int, int, char> p8(1,2,'C');
    s1.removePoint(p8);
    std::cout <<"set1 - "<< p8 <<  std::endl;
    std::cout <<s1<< std::endl;
    s1.removePoint(p1);
    std::cout <<"set1 - "<<  p1 <<  std::endl;
    std::cout <<s1<< std::endl;
    s1.addPoint(p1);
    s1.addPoint(p3);
    std::cout <<"set1:"<< std::endl;
    std::cout <<s1<< std::endl;
    Point<int, int, char> p11(2,2,'O');
    Point<int, int, char> p12(1,2,'a');
    Point<int, int, char> p13(2,3,':');
    Point<int, int, char> p14(2,3,'k');
    Point<int, int, char> p15(0,1,'*');
    Point<int, int, char> p16(2,0,'0');
    Point<int, int, char> p17(3,2,'z');
    std::cout <<p11<<"  "<<p12<<"  "<<p13<<"  "<<p14<<"  "<<p15<<"  "<<p16<<"  "<<p17<<"  "<< std::endl;
    SetOfPoints<int, int, char> s2;
    s2.addPoint(p11);
    s2.addPoint(p12);
    s2.addPoint(p13);
    s2.addPoint(p14);
    s2.addPoint(p15);
    s2.addPoint(p16);
    s2.addPoint(p17);
    std::cout <<"set2:"<< std::endl;
    std::cout <<s2<< std::endl;
    SetOfPoints<int, int, char> s3=s1+s2;
    std::cout <<"set3=s1+s2"<< std::endl;
    std::cout <<s3<< std::endl;
    SetOfPoints<int, int, char> s4=s1*s2;
    std::cout <<"set4=s1*s2"<< std::endl;
    std::cout <<s4<< std::endl;

}

Test3::~Test3()
{
}

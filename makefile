TARGET = saper.cpp
CC = g++

$(TARGET): main.o Point.o SetOfPoints.o Test.o Test2.o Test3.o Board.o Interface.o
	$(CC) main.o Point.o SetOfPoints.o Test.o Test2.o Test3.o Board.o Interface.o -o $(TARGET)

main.o: main.cpp Point.h SetOfPoints.h Test.h Test2.h Test3.h Board.h Interface.h
	$(CC) main.cpp -c -o main.o

Board.o: Board.cpp Board.h SetOfPoints.h Point.h
	$(CC) Board.cpp -c -o Board.o

SetOfPoints.o: SetOfPoints.cpp SetOfPoints.h Point.h
	$(CC) SetOfPoints.cpp -c -o SetOfPoints.o

Test.o: Test.cpp Test.h SetOfPoints.h Point.h
	$(CC) Test.cpp -c -o Test.o

Test2.o: Test2.cpp Test.h SetOfPoints.h Point.h
	$(CC) Test2.cpp -c -o Test2.o

Test3.o: Test.cpp Test3.h SetOfPoints.h Point.h
	$(CC) Test3.cpp -c -o Test3.o

Point.o: Point.cpp Point.h
	$(CC) Point.cpp -c -o Point.o

Interface.o: Interface.cpp Interface.h
	$(CC) Interface.cpp -c -o Interface.o

.PHONY: clean
clean:
	rm -f *.o
	rm -f $(TARGET)


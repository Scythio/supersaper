#include "Board.h"
#include <iostream>
#include "Point.h"
#include "SetOfPoints.h"
#include <time.h>

Board::Board(int row,int col)
{
    //SURFACE & UNDERGROUND CREATION
    rows=row;
    columns=col;
	int r = 0, c = 0;
	while(r<row)
	{
        c = 0;
        while(c<col)
        {
            Point<int,int,char> pS(r,c,'X');
            Point<int,int,int> pU(r,c,0);
            surface.addPoint(pS);
            underground.addPoint(pU);
            ++c;
        }
        ++r;
    }
    //UNDERGROUND RANDOM MINES
    int mines=row*col/12;
    int rm=0, cm=0, m=0;
    srand( time(NULL) );
    do{
		rm =rand()%row;
		cm =rand()%col;
		std::cout << "    rm " << rm << "    cm " << cm << std::endl;
		//underground.findPoint(rm,cm)
		if(underground.findPoint(rm,cm)->z==0 )
		{
			underground.findPoint(rm,cm)->z=1;
			++m;
		}

	}while(m<mines);
}

void Board::draw()  //const SetOfPoints<int,int,char>& board | SetOfPoints<int,int,char>::
{
    Element<int,int,char> *target=surface.head;// *target=surface.head;  //<int,int,char>
    if(target!=NULL)
    {
        std::cout << ".      ";
        for(int c=0;c<columns;++c)
        {
            if(c<10)
            {
                std::cout << ". " << c << " ";
            }
            else
            {
                std::cout << ". " << c;
            }
        }
        std::cout << "." << std::endl << std::endl;
        for(int r=0;r<rows;++r)
        {

            std::cout << ".      ";

            for(int c=0;c<columns;++c)
            {
                std::cout << ".___";
            }
            std::cout << "." << std::endl;

            if(r<10)
            {
                std::cout << ". " << r << "    ";
            }
            else
            {
                std::cout << ". " << r << "   ";
            }

            for(int c=0;c<columns;++c)
            {
                if(target!=NULL)
                {
                    std::cout << "| " << target->point.z << " ";
                    target=target->next;
                }
                else
                {
                    std::cerr<<"error";
                    exit(EXIT_FAILURE);
                }
            }
            std::cout << "|" << std::endl;
        }
        std::cout << ".      ";
        for(int c=0;c<columns;++c)
        {
            std::cout << ".___";
        }
        std::cout << "." << std::endl;
    }

    //std::cout << "wyraz 1 wyraz 2 wyraz 3..." <<std::endl;
    //std::cout << surface <<std::endl;
    //std::cout << surface <<std::endl;
    //std::cout << mmm;
}

bool Board::play(int x, int y, char task)
{
    switch (task)
    {
        case 's':
            //char symbol = surface.findPoint(x,y)->z;
            if(surface.findPoint(x,y)->z=='X' || surface.findPoint(x,y)->z=='.' || surface.findPoint(x,y)->z=='!' || surface.findPoint(x,y)->z=='?')
            {
                if(underground.findPoint(x,y)->z==0)
                {
                    radar(x,y);
                    return 0;
                }
                else
                {
                    showMines();
                    surface.findPoint(x,y)->z='@';
                    return 1; // <-BOOOM!!!
                }
            }
            break;
        case 'm':
            showMines();
            return 0;
            break;
        case '!':
            //char symbol2 = surface.findPoint(x,y)->z;
            if(surface.findPoint(x,y)->z=='X' || surface.findPoint(x,y)->z=='.' || surface.findPoint(x,y)->z=='?')
            {
                surface.findPoint(x,y)->z='!';
            }
            else if(surface.findPoint(x,y)->z=='!')
            {
                surface.findPoint(x,y)->z='X';
            }
            return 0;
            break;
        case '?':
            if(surface.findPoint(x,y)->z=='X' || surface.findPoint(x,y)->z=='.' || surface.findPoint(x,y)->z=='!')
            {
                surface.findPoint(x,y)->z='?';
            }
            else if(surface.findPoint(x,y)->z=='?')
            {
                surface.findPoint(x,y)->z='X';
            }
            return 0;
            break;
        default:
            return 0;
            break;
    }
    return 0;
}
/*

    if(underground.findPoint(x,y)->z==0)
    {

    }
*/
Board::~Board()
{
    //dtor
}

bool Board::wonTheGame(void)
{
    //over every mine is flag
    //under every flag is mine
    for(int r=0;r<rows;++r)
    {
        for(int c=0;c<columns;++c)
        {
            if(underground.findPoint(r,c)!=NULL  &&  surface.findPoint(r,c)!=NULL)
            {
                if(underground.findPoint(r,c)->z==1  &&  surface.findPoint(r,c)->z!='!')
                {
                    return 0; //over NOT every mine is flag
                }
                else if(underground.findPoint(r,c)->z!=1  &&  surface.findPoint(r,c)->z=='!')
                {
                    return 0; //under NOT every flag is mine
                }
            }
            else
            {
                std::cerr<<"error outside SetOfPoints";
                exit(EXIT_FAILURE);
            }
        }
    }
    return 1;
}


void Board::showMines()
{
    for(int r=0;r<rows;++r)
    {
        for(int c=0;c<columns;++c)
        {
            if(underground.findPoint(r,c)!=NULL  &&  surface.findPoint(r,c)!=NULL)
            {
                if(underground.findPoint(r,c)->z==1 )
                {
                    surface.findPoint(r,c)->z='#';
                }
                else if(underground.findPoint(r,c)->z>1 )
                {
                    surface.findPoint(r,c)->z='V';
                }
                else if(surface.findPoint(r,c)->z=='X' )
                {
                    surface.findPoint(r,c)->z='.';
                }
            }
            else
            {
                std::cerr<<"error outside SetOfPoints";
                exit(EXIT_FAILURE);
            }
        }
    }
}


void Board::radar(int x, int y)
{
    if(x>=0 && x<rows && y>=0 && y<columns)    //if(underground.findPoint(x,y)!=NULL)
    {
        if(surface.findPoint(x,y)->z=='X' || surface.findPoint(x,y)->z=='.')
        {
            if(underground.findPoint(x,y)->z==0)
            {
                int m = countMines(x,y);
                if(m>0)
                {
                    surface.findPoint(x,y)->z=m+48;
                }
                else
                {
                    surface.findPoint(x,y)->z=' ';
                    radar(x-1,y-1);
                    radar(x-1,y);
                    radar(x-1,y+1);
                    radar(x,y-1);
                    radar(x,+1);
                    radar(x+1,y-1);
                    radar(x+1,y);
                    radar(x+1,y+1);
                }
            }
        }
    }
}

int Board::isMine(int r, int c)
{
    if(r>=0 && r<rows && c>=0 && c<columns) //if(underground.findPoint(x-1,y-1)!=NULL)
    {
        return underground.findPoint(r,c)->z;
    }
    return 0;
}

int Board::countMines(int x, int y)
{
    int sum = 0;
    sum+=isMine(x-1,y-1);
    sum+=isMine(x-1,y);
    sum+=isMine(x-1,y+1);
    sum+=isMine(x,y-1);
    sum+=isMine(x,y+1);
    sum+=isMine(x+1,y-1);
    sum+=isMine(x+1,y);
    sum+=isMine(x+1,y+1);
    return sum;
}


            /*std::cout << "countMines=0" << std::endl;
            surface.findPoint(x,y)->z=' ';
            std::cout << "radar(x-1,y-1);" << std::endl;
            radar(x-1,y-1);
            std::cout << "radar(x-1,y);" << std::endl;
            radar(x-1,y);
            std::cout << "radar(x-1,y+1);" << std::endl;
            radar(x-1,y+1);
            std::cout << "radar(x,y-1);" << std::endl;
            radar(x,y-1);
            std::cout << "radar(x,+1);" << std::endl;
            radar(x,+1);
            std::cout << "radar(x+1,y-1);" << std::endl;
            radar(x+1,y-1);
            std::cout << "radar(x+1,y);" << std::endl;
            radar(x+1,y);
            std::cout << "radar(x+1,y+1);" << std::endl;
            radar(x+1,y+1);*/

/*
void Board::radar(int x, int y)
{
    if(underground.findPoint(x-1,y-1)!=NULL)
    {
        if(underground.findPoint(x-1,y-1)->z==0)
        {
            surface.findPoint(x-1,y-1)->z=countMines(x-1,y-1);
        }
    }
    if(underground.findPoint(x-1,y)!=NULL)
    {
        if(underground.findPoint(x-1,y)->z==0)
        {
            surface.findPoint(x-1,y)->z=countMines(x-1,y);
        }
    }
    if(underground.findPoint(x-1,y+1)!=NULL)
    {
        if(underground.findPoint(x-1,y+1)->z==0)
        {
            surface.findPoint(x-1,y+1)->z=countMines(x-1,y+1);
        }
    }
    if(underground.findPoint(x,y-1)!=NULL)
    {
        if(underground.findPoint(x,y-1)->z==0)
        {
            surface.findPoint(x,y-1)->z=countMines(x,y-1);
        }
    }
    if(underground.findPoint(x,y+1)!=NULL)
    {
        if(underground.findPoint(x,y+1)->z==0)
        {
            surface.findPoint(x,y+1)->z=countMines(x,y+1);
        }
    }
    if(underground.findPoint(x+1,y-1)!=NULL)
    {
        if(underground.findPoint(x+1,y-1)->z==0)
        {
            surface.findPoint(x+1,y-1)->z=countMines(x+1,y-1);
        }
    }
    if(underground.findPoint(x+1,y)!=NULL)
    {
        if(underground.findPoint(x+1,y)->z==0)
        {
            surface.findPoint(x+1,y)->z=countMines(x+1,y);
        }
    }
    if(underground.findPoint(x+1,y+1)!=NULL)
    {
        if(underground.findPoint(x+1,y+1)->z==0)
        {
            surface.findPoint(x+1,y+1)->z=countMines(x+1,y+1);
        }
    }
}
*/

/*
    if(r>=0 && r<rows && c>=0 && c<columns) //if(underground.findPoint(x-1,y-1)!=NULL)
    {
        sum=sum+underground.findPoint(x,y)->z;
    }
    if(underground.findPoint(x-1,y)!=NULL)
    {
        sum=sum+underground.findPoint(x-1,y)->z;
    }
    if(underground.findPoint(x-1,y+1)!=NULL)
    {
        sum=sum+underground.findPoint(x-1,y+1)->z;
    }
    if(underground.findPoint(x,y-1)!=NULL)
    {
        sum=sum+underground.findPoint(x,y-1)->z;
    }
    if(underground.findPoint(x,y+1)!=NULL)
    {
        sum=sum+underground.findPoint(x,y+1)->z;
    }
    if(underground.findPoint(x+1,y-1)!=NULL)
    {
        sum=sum+underground.findPoint(x+1,y-1)->z;
    }
    if(underground.findPoint(x+1,y)!=NULL)
    {
        sum=sum+underground.findPoint(x+1,y)->z;
    }
    if(underground.findPoint(x+1,y+1)!=NULL)
    {
        sum=sum+underground.findPoint(x+1,y+1)->z;
    }
    //KONWERSJA int -> char
    //char c = static_cast<char>(sum);
    //char c = (char)sum;
    //std::cout << "Hau Hau" << sum+48 << std::endl;
    return sum;
*/
//NIEWYPAŁY:

/*

        while(target!=NULL  &&  target->point.y != 0)
        {


        }

        if(target!=NULL)
        {
            std::cout << std::endl << target->point.z;
            target=target->next;
        }
*/

#ifndef TEST2_H
#define TEST2_H


class Test2
{
    public:
        Test2();
        void pointOperators(void);
        void setOfPoints(void);
        virtual ~Test2();

    //protected:
    private:
};

#endif // TEST2_H

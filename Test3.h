#ifndef TEST3_H
#define TEST3_H


class Test3
{
    public:
        Test3();
        void pointOperators(void);
        void setOfPoints(void);
        void pointFunctions(void);
        virtual ~Test3();

    //protected:
    private:
};

#endif // TEST3_H

#include <iostream>
#include <stdio.h>
#include "Point.h"
#include "Test.h"
#include "Test2.h"
#include "Test3.h"
#include "Board.h"
#include "Interface.h"
//#include <string.h>

using namespace std;
void peep();

int main()
{
/*
    cout<< "<float>" <<endl;	//TESTS
    Test test;
    test.pointOperators();
    cout<<endl;
    test.setOfPoints();

    cout<< "<char>" <<endl;
    Test2 test2;
    test2.pointOperators();
    cout<<endl;
    test2.setOfPoints();

    cout<< "<int, int, char>" <<endl;
    Test3 test3;
    test3.pointOperators();
    cout<<endl;
    test3.setOfPoints();
    cout<<endl;
    test3.pointFunctions();
*/
    /*
    int m = 6;
    char c, g;
    c = 7;
    g='5';
    cout << "int " << m << "    char c " << c << "    char g " << g <<endl;
    peep();
    */
    bool  again=0;
    do{
        Interface interface;
        interface.start();
        cout<<endl;
        Board game(interface.row,interface.col);
        bool win=0, loss=0;
        do{
            system("clear");
            game.draw();
            interface.control();
            loss=game.play(interface.x,interface.y,interface.task);
            win=game.wonTheGame();
            system("clear");
            game.draw();
        }while(win==0 && loss==0);
        again=interface.theEnd(win);
    }while(again == 1);
    return 0;
}




/*
cout << "Hello world!" << endl;
    Point p1;
    p1.coord(1,6,3);
    cout << "p1  " << p1 << endl;
    Point p2;
    p2.coord(2,-4.5,0.4);
    cout << "p2  " << p2 << endl;
    Point p3=p1-p2;
    cout << "p3  " << p3 << endl;
    p3+=p2;
    cout << "p3  " << p3 << endl;
*/

#ifndef BOARD_H
#define BOARD_H
#include <iostream>
#include "Point.h"
#include "SetOfPoints.h"

class Board
{
    private:
        int mmm=16;
        SetOfPoints<int,int,char> surface;
        SetOfPoints<int,int,int> underground;
        int rows, columns;

        void showMines();
        void radar(int x, int y);
        int countMines(int x, int y);
        int isMine(int r, int c);

    public:
        Board(int row, int col);
        void draw(void);    //const SetOfPoints<int,int,char>& board
        bool play(int x, int y, char task);
        bool wonTheGame(void);

        virtual ~Board();
    protected:

};

#endif // BOARD_H

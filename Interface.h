#ifndef INTERFACE_H
#define INTERFACE_H
#include <iostream>

class Interface
{
    private:
    public:
        int x, y, row, col;
        char task;
        Interface();
        void menu();
        void start();
        void control();
        bool theEnd(bool win);
        virtual ~Interface();
    protected:


};

#endif // INTERFACE_H

#include "Point.h"

//template <class T1, class T2, class T3> class Point;


template <class T1,class T2,class T3>
Point<T1,T2,T3>::Point(T1 xx,T2 yy,T3 zz)
{
    x=xx;   y=yy;   z=zz;
}

template <class T1,class T2,class T3>
void Point<T1,T2,T3>::modify(T1 xx,T2 yy,T3 zz)
{
    x=xx;   y=yy;   z=zz;
}

template <class T1, class T2, class T3>
Point<T1,T2,T3> Point<T1,T2,T3>::operator+(const Point<T1,T2,T3>& p) const
{
    Point p2;//<T1,T2,T3>
    p2.x=(this->x + p.x);
    p2.y=(this->y + p.y);
    p2.z=(this->z + p.z);
    return p2;
}

template <class T1, class T2, class T3>
Point<T1,T2,T3> Point<T1,T2,T3>::operator-(const Point<T1,T2,T3>& p) const
{
    Point p2;//<T1,T2,T3>
    p2.x=(this->x - p.x);
    p2.y=(this->y - p.y);
    p2.z=(this->z - p.z);
    return p2;
}

template <class T1, class T2, class T3>
Point<T1, T2, T3> Point<T1, T2, T3>::operator+=(const Point<T1, T2, T3>& p)
{
    this->x += p.x;
    this->y += p.y;
    this->z += p.z;
    return *this;
}

template <class T1, class T2, class T3>
Point<T1, T2, T3> Point<T1, T2, T3>::operator-=(const Point<T1, T2, T3>& p)
{
    this->x -= p.x;
    this->y -= p.y;
    this->z -= p.z;
    return *this;
}

template <class T1, class T2, class T3>
Point<T1, T2, T3> Point<T1, T2, T3>::operator=(const Point<T1, T2, T3>& p)
{
    this->x = p.x;
    this->y = p.y;
    this->z = p.z;
    return *this;
}

template <class T1, class T2, class T3>
bool Point<T1, T2, T3>::operator==(const Point<T1, T2, T3>& p)
{
    if( this->x==p.x  &&  this->y==p.y  &&  this->z==p.z )
    {
    return true;
    }
    else
    {
    return false;
    }
}

template <class T1, class T2, class T3>
bool Point<T1, T2, T3>::operator!=(const Point<T1, T2, T3>& p)
{
    if( this->x!=p.x  ||  this->y!=p.y  ||  this->z!=p.z )
    {
    return true;
    }
    else
    {
    return false;
    }
}

template <class T1, class T2, class T3>
bool Point<T1, T2, T3>::operator<(const Point<T1, T2, T3>& p)
{
    if(this->x==p.x)
    {
        if(this->y==p.y)
        {
            if(this->z>=p.z)
            {
                return false;
            }
            else if( this->z<p.z )
            {
                return true;
            }
        }
        else if( this->y<p.y )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    else if( this->x<p.x )
    {
        return true;
    }
    else
    {
        return false;
    }
}

template <class T1, class T2, class T3>
bool Point<T1, T2, T3>::operator>(const Point<T1, T2, T3>& p)
{
    if(this->x==p.x)
    {
        if(this->y==p.y)
        {
            if(this->z<=p.z)
            {
                return false;
            }
            else
            {
                if( this->z>p.z )
                {
                    return true;
                }
            }
        }
        else
        {
            if( this->y>p.y )
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
    else
    {
        if( this->x>p.x )
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
/*
template <typename Type>
std::ostream& operator<<(std::ostream& os, const Point<T1, T2, T3>& p)
{
    os << p.x << ' ' << p.y << ' ' << p.z;
    return os;
}
*/
template <class T1, class T2, class T3>
Point<T1, T2, T3>::~Point<T1, T2, T3>()
{
    //dtor
}


template class Point<float,float,float>;    //to potrzebne, bo inaczej error: "undefined reference to..."
template class Point<char, char, char>;
template class Point<int, int, char>;
template class Point<int, int, int>;

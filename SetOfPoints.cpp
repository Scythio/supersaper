#include <iostream>
#include "SetOfPoints.h"
#include "Point.h"

//template<typename Type> class Point<T1, T2, T3>;
//template <class Type> class Point;
//Point<float> point;
template <class T1,class T2, class T3>
SetOfPoints<T1, T2, T3>::SetOfPoints()
{
      head=NULL;
      tail=NULL;
}

template <class T1,class T2, class T3>
void SetOfPoints<T1, T2, T3>::addPoint(Point<T1, T2, T3> newPoint)
{
    Element<T1, T2, T3> *newElement=new Element<T1, T2, T3>;
    Element<T1, T2, T3> *target=head;
    Element<T1, T2, T3> *prev=head;
    newElement->point=newPoint;
    newElement->next=NULL;
    bool ready=0;
    if(head==NULL)
    {
        head=newElement;
        tail=newElement;
        newElement=NULL;
    }
    else
    {
        while(target!=NULL && !ready)
        {
            if( target->point > newElement->point )
            {
                newElement->next=target;
                if(target==head)
                {
                    head=newElement;
                }
                else
                {
                    prev->next=newElement;
                }
                ready=true;
            }
            else if( target->point == newElement->point )
            {
                ready=true;
                delete newElement;
            }
            if(!ready)
            {
            prev=target;
            target=target->next;
            }
        }
        if(!ready){
            tail->next=newElement;
            tail=newElement;
        }
    }
}

template <class T1,class T2, class T3>
void SetOfPoints<T1, T2, T3>::removePoint(Point<T1, T2, T3> point2remove)
{
    Element<T1, T2, T3> *target=head;
    Element<T1, T2, T3> *prev=NULL;
    bool ready=0;
    while(target!=NULL && !ready)
    {
        if( target->point == point2remove)
        {
            if(target==head)
            {
                head=target->next;
            }
            else
            {
                prev->next=target->next;
            }
            ready=true;
            delete target;
        }
        else if( target->point > point2remove )
        {
            ready=true;
            std::cout<< "There was no such a point to be removed." <<std::endl;
        }
        if(!ready)
        {
        prev=target;
        target=target->next;
        }
    }
    if(!ready){
        std::cout<< "There was no such a point to be removed." <<std::endl;
    }

}

template <class T1,class T2, class T3>
Point<T1, T2, T3>* SetOfPoints<T1, T2, T3>::findPoint(T1 xx, T2 yy)
{
    Element<T1, T2, T3> *target=head;
    //Element<T1, T2, T3> *prev=NULL;
    //bool ready=0;
    while(target!=NULL) //&& !ready
    {
        if( target->point.x == xx   &&  target->point.y == yy)
        {
            return &target->point;
        }
        else
        {
            target=target->next;
        }
    }
    return NULL;
}
/*
std::ostream& operator<<(std::ostream& os, const SetOfPoints& s)
{
    Element *target=s.head;
    while(target!=NULL)
    {
        os << target->point <<std::endl;
        target=target->next;
    }
    return os;
}
*/
template <class T1,class T2, class T3>
SetOfPoints<T1, T2, T3> SetOfPoints<T1, T2, T3>::operator+(const SetOfPoints<T1, T2, T3>& s) const
{
    SetOfPoints<T1, T2, T3> s2;
    Element<T1, T2, T3> *target=this->head; //.head
    while(target!=NULL)
    {
        s2.addPoint(target->point);
        target=target->next;
    }
    target=s.head; //.head
    while(target!=NULL)
    {
        s2.addPoint(target->point);
        target=target->next;
    }
    return  s2;
}

template <class T1,class T2, class T3>
SetOfPoints<T1, T2, T3> SetOfPoints<T1, T2, T3>::operator*(const SetOfPoints<T1, T2, T3>& s) const
{
    SetOfPoints<T1, T2, T3> s2;
    Element<T1, T2, T3> *target1=this->head; //.head
    Element<T1, T2, T3> *target2=s.head; //.head
    while(target1!=NULL)
    {
        target2=s.head;
        while(target2!=NULL)
        {
            if(target1->point==target2->point)
            {
                s2.addPoint(target1->point);
            }
            target2=target2->next;
        }
        target1=target1->next;
    }
    return  s2;
}

template <class T1,class T2, class T3>
SetOfPoints<T1, T2, T3> SetOfPoints<T1, T2, T3>::operator+=(const SetOfPoints<T1, T2, T3>& s)
{
    *this = *this+s;
    return *this;
}

template <class T1,class T2, class T3>
SetOfPoints<T1, T2, T3> SetOfPoints<T1, T2, T3>::operator*=(const SetOfPoints<T1, T2, T3>& s)
{
    *this = *this*s;
    return *this;
}

template <class T1,class T2, class T3>
SetOfPoints<T1, T2, T3>::~SetOfPoints()
{
    Element<T1, T2, T3> *target=head;
    Element<T1, T2, T3> *ex=NULL;
    while(target!=NULL)
    {
        ex=target;
        target=target->next;
        delete ex;
    }
}

template struct Element<float, float, float>;     //to potrzebne, bo inaczej error: "undefined reference to..."
template class SetOfPoints<float, float, float>;
template struct Element<char,char,char>;     //to potrzebne, bo inaczej error: "undefined reference to..."
template class SetOfPoints<char,char,char>;
template struct Element<int,int,char>;
template class SetOfPoints<int,int,char>;
template struct Element<int,int,int>;
template class SetOfPoints<int,int,int>;

//NIEWYPAŁY:
/*
template <class T1,class T2, class T3>
SetOfPoints<T1, T2, T3>::Element<T1, T2, T3>* getHead()
{
    return head;
}
*/
